import React from "react";
// javascript plugin used to create scrollbars on windows
import PerfectScrollbar from "perfect-scrollbar";
import Footer from "../components/Footer.jsx";
import ThemeProvider from "../utils/themeContext/ThemeProvider.jsx";

var ps;

export default class AuthLayout extends React.Component {

    componentDidMount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps = new PerfectScrollbar(this.refs.fullPages);
        }
    }

    componentWillUnmount() {
        if (navigator.platform.indexOf("Win") > -1) {
            ps.destroy();
        }
    }

    render() {
        return (
            <>
                <ThemeProvider>
                    <div className="wrapper wrapper-full-page" ref="fullPages">
                        <div className="full-page section-image">
                            {this.props.children}
                            <Footer fluid/>
                        </div>
                    </div>
                </ThemeProvider>
            </>
        );
    }
}