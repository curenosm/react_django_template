import React from "react";
import {connect} from 'react-redux';

import {loginUser} from "./../../redux/actions/auth";


class Login extends React.Component {

    state = {
        'username': '',
        'password': ''
    }

    componentDidMount() {
        document.body.classList.toggle("login-page");
    }

    componentWillUnmount() {
        document.body.classList.toggle("login-page");
    }

    handleChange = (event) => {
        event.preventDefault();
        this.setState({
            [event.target.name]: event.target.value
        });
    }

    handleSubmit = (event) => {
        event.preventDefault();
        let res = this.props.loginUser(this.state.username, this.state.password);
    }

    render() {
        return (
            <div className="row">
                <div className="col col-sm-12 pl-5 pr-5 pt-5 mb-5">
                    <form className="form" method="POST">
                        <div className="form-group">
                            <label htmlFor="username">Username/Email:</label>
                            <input
                                className="form-control"
                                value={this.state.username}
                                name="username"
                                type="text"
                                onChange={this.handleChange}
                                placeholder="Username"
                                ></input>
                            <div className="error-input">
                                {this.props.authentication.error?.username}
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="password">Password:</label>
                            <input
                                className="form-control"
                                value={this.state.password}
                                name="password"
                                type="password"
                                placeholder="Password"
                                onChange={this.handleChange}
                                autoComplete="off"
                                ></input>
                            <div className="error-input">
                                {this.props.authentication.error?.password || this.props.authentication.error?.__all__}
                            </div>
                        </div>
                        <button
                            className="btn btn-block btn-primary"
                            onClick={this.handleSubmit}
                            >Login</button>
                    </form>
                </div>
            </div>
        );
    }
}


const mapDispatchToProps = {
    loginUser
};

const mapStateToProps = state => ({
    authentication: state.auth
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
