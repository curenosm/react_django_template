import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux';

import { postUser } from '../../redux/actions/users';


class Form extends Component {

    static propTypes = {
        postUser: PropTypes.func.isRequired
    }

    state = {
        'username': '',
        'email': '',
        'last_name': '',
        'first_name': '',
        'password': '',
        'password2': ''
    }

    onSubmit = e => {
        e.preventDefault();
        const {
            username,
            email,
            first_name,
            last_name,
            password,
            password2
        } = this.state;
        this.props.postUser({
            username,
            email,
            first_name,
            last_name,
            password,
            password2
        });
    }

    onChange = e => {
        e.preventDefault();
        this.setState({
            [e.target.name]: e.target.value
        });
    }

    render() {
        const {
            username,
            email,
            first_name,
            last_name,
            password,
            password2
        } = this.state;
        return (
            <div className="card card-body mt-4 mb-4">
                <form autocomplete="false" onSubmit={this.onSubmit}>
                    <div className="form-group">
                        <label>Username:</label>
                        <input
                        className="form-control"
                        type="text"
                        name="username"
                        onChange={this.onChange}
                        value={username}/>
                    </div>
                    <div className="form-group">
                        <label>Email:</label>
                        <input
                        className="form-control"
                        type="email"
                        name="email"
                        onChange={this.onChange}
                        value={email}/>
                    </div>
                    <div className="form-group">
                        <label>First name:</label>
                        <input
                        className="form-control"
                        type="text"
                        name="first_name"
                        onChange={this.onChange}
                        value={first_name}/>
                    </div>
                    <div className="form-group">
                        <label>Last name:</label>
                        <input
                        className="form-control"
                        type="text"
                        name="last_name"
                        onChange={this.onChange}
                        value={last_name}/>
                    </div>
                    <div className="form-group">
                        <label>Password:</label>
                        <input
                        className="form-control"
                        type="password"
                        name="password"
                        onChange={this.onChange}
                        value={password}/>
                    </div>
                    <div className="form-group">
                        <label>Password (confirmation):</label>
                        <input
                        className="form-control"
                        type="password"
                        name="password2"
                        onChange={this.onChange}
                        value={password2}/>
                    </div>
                    <button
                        className="btn btn-block btn-info"
                        type="submit">
                        Crear
                    </button>
                </form>
            </div>
        )
    }
}

export default connect(null, { postUser })(Form);