import React, { Component } from 'react';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { getUsers, deleteUser } from '../../redux/actions/users';


class Users extends Component {

    static propTypes = {
        users: PropTypes.array.isRequired,
        getUsers: PropTypes.func.isRequired,
        deleteUser: PropTypes.func.isRequired
    };

    componentDidMount(){
        this.props.getUsers();
    }

    render(){
        return (
            <>
                <h2>Users</h2>
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Username</th>
                            <th>Email</th>
                            <th>First</th>
                            <th>Last</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.props.users.map(user =>
                            <tr key={user.id}>
                                <td>{user.id}</td>
                                <td>{user.username}</td>
                                <td>{user.email}</td>
                                <td>{user.first_name}</td>
                                <td>{user.last_name}</td>
                                <td>
                                    <button 
                                        className="btn btn-danger btn-sm"
                                        onClick={this.props.deleteUser.bind(this, user.id)}>
                                            X
                                    </button>
                                </td>
                            </tr>    
                        )}
                    </tbody>
                </table>
            </>
        );
    }
}

const mapStateToProps = state => ({
    something: state.user.something,
    users: state.user.users
})

export default connect(
    mapStateToProps,
    { getUsers, deleteUser }
)(Users);