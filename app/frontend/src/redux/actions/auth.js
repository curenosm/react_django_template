import {LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT} from './types';
import {authenticate, get_initial_session_status, logout, AuthenticationFormError} from '../../utils/auth';


export function loginUser(username, password) {
    console.log('Aquí llegó');
    return async function (dispatch){
        dispatch({
            type: LOGIN_REQUEST,
            username
        });

        var credentials;
        try {
            credentials = await authenticate(username, password);
            console.log(credentials);
            credentials.permissions = new Set(credentials.permissions);
        } catch(err) {
            if(err instanceof AuthenticationFormError){
                dispatch({
                    type: LOGIN_FAILURE,
                    error: err.form_errors
                });
            } else {
                dispatch({
                    type: LOGIN_FAILURE,
                    error: {__all__:[err.toString()]}
                });
            }
            return;
        }

        dispatch({
            type: LOGIN_SUCCESS,
            credentials
        });
    };
}

export function logoutUser() {
    return async function (dispatch){
        try {
            logout();
        } catch(err) {
            console.log(err);
            //TODO: When we have a standard error bar or toast dispatch error message action here
            return;
        }

        dispatch({
            type: LOGOUT
        });
    }
}

//Checks if we are still logged in with the backend
export function checkSession() {
    return async function (dispatch){
        try {
            await get_initial_session_status();
        } catch {
            //Assume we are not logged-in
            dispatch({
                type: LOGOUT
            });
        }
    }
}
