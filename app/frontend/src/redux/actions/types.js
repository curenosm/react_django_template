// AUTH
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_FAILURE = 'LOGIN_FAILURE';
export const LOGOUT = 'LOGOUT';

// USERS
export const GET_USERS = 'GET_USERS';
export const DELETE_USER = 'DELETE_USER';
export const POST_USER = 'POST_USER';