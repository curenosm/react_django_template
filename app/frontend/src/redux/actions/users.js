import axios from 'axios';

import { GET_USERS, DELETE_USER, POST_USER } from './types';

// GET USERS
export const getUsers = () => dispatch => {
    axios.get('/api/users/')
        .then(res => {
            dispatch({
                type: GET_USERS,
                payload: res.data
            });
        }).catch(e => console.log(e));
};

// DELETE USER
export const deleteUser = (id) => dispatch => {
    axios.delete(`/api/users/${id}/`)
        .then(res => {
            dispatch({
                type: DELETE_USER,
                payload: id
            });
        }).catch(e => console.log(e));
};

// POST USER
export const postUser = (user) => dispatch => {
    axios.post(`/api/users/`, user)
        .then(res => {
            dispatch({
                type: POST_USER,
                payload: res.data,
            })
        }).catch(e => console.log(e));
}