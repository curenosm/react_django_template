import { LOGIN_REQUEST, LOGIN_SUCCESS, LOGIN_FAILURE, LOGOUT } from "../actions/types";

const initialState = {
    id: null,
    username: 'anonymous',
    isSuperuser: false,
    permissions: new Set(),
    groups: [],
    status: 'Initial',
    error: {}
};

const loggedOutState = {
    ...initialState,
    status: 'LoggedOut'
};


export default function auth (state = initialState, action) {
    switch (action.type) {
        case LOGIN_REQUEST:
            return { 
                ...state,
                username: action.username,  
                status: "LoginInProgress",
                error: {}
            };
        case LOGIN_SUCCESS:
            return {
                ...action.credentials, 
                status:"LoggedIn"
            };
        case LOGIN_FAILURE:
            return {
                ...loggedOutState,
                ...action, //adds the error to the logged out state
            };
        case LOGOUT:
            return {
                ...loggedOutState
            };
        default:
            return state
    }
}

