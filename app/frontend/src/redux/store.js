import { createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';

import {get_initial_session_status, AuthenticationError} from '../utils/auth';
import rootReducer from './reducers/index';

const storeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;


var store = null;


async function  initialize_store(){

    try{
        store = createStore(
            rootReducer,
            {
                "auth": {
                    ...await get_initial_session_status(), //Throws AuthenticationError if the user is not logged in.
                    status:"LoggedIn"
                }
            },
            storeEnhancers(applyMiddleware(thunk))
        );
    } catch(err){
        //If there was an exception above, the store is not created with an initialized session in authReducer.
        //Instead create the store with default values (logged-out)
        if(!(err instanceof AuthenticationError && err.message === "not logged in")){
            console.log(err); //log other unexpected error
        }
        store = createStore(
            rootReducer,
            storeEnhancers(applyMiddleware(thunk))
        );
    }
    return store;
}



export default initialize_store;