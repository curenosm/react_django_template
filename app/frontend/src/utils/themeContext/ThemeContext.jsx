import * as React from "react";

export const ThemeContext = React.createContext(
    {
        themeContext: {
            brand: '',
            setBrand: () => {},
            routes: [],
            setRoutes: () => {}
        }
    }
)