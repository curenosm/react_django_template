import * as React from 'react';
import {ThemeContext} from './ThemeContext.jsx';


export default function applyTheme(Component) {

    return function ThemeComponent(props) {
        return (
            <ThemeContext.Consumer>
                {(contexts) => <Component {...props} {...contexts} />}
            </ThemeContext.Consumer>
        )
    }
}