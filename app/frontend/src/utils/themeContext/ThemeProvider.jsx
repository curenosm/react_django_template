import * as React from 'react';
import { connect } from 'react-redux';

import applyTheme from './ApplyTheme.jsx';
import {ThemeContext} from './ThemeContext.jsx';
import {history} from '../history';
import {routesOne, routesTwo} from '../../routes';

class ThemeProvider extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            setBrand: this.setBrand.bind(this),
            brand: '',
            routes: routesOne,
            setRoutes: this.setRoutes.bind(this),

        };
    }

    setBrand(brand) {
        console.log("Set brand", +brand);
        if(this.setRoutes(brand)){
            this.setState({brand: brand});
            return true;
        }
        return false;
    }

    setRoutes(brand) {
        let routes = [];
        let hasPermission = false;

        switch(brand){
            case "brandOne":
                if (this.props.permissions.has("permission1")){

                    hasPermission = true;

                    if(hasPermission === true){
                        routes = routesSynopsis;
                    } 
                }
                break;
            case "brandTwo":
                if(this.props.permissions.has("permission2")){
                    routes = routesMultip;
                    hasPermission = true;
                }
                break;
            default:
                break;
        }

        if(!hasPermission){
            history.push("/products");
            return false;
        }

        this.setState({routes: routes});
        return true;
    }

    render() {
        return (
            <ThemeContext.Provider value={{
                themeContext: {
                    ...this.state
                }
            }}>
                {this.props.children}
            </ThemeContext.Provider>
        )
    }
}

const mapStateToProps = state => ({
    permissions: state.auth.permissions,
});

const componentWithContext = applyTheme(ThemeProvider);
export default connect(mapStateToProps)(componentWithContext);