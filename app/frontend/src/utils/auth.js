export function getCookie(name){
    let cookieValue = null;
    if(document.cookie && document.cookie !== ''){
        let cookies = document.cookie.split(';');
        for(let i = 0; i < cookies.length; i++){
            let cookie = cookies[i].trim();
            if (cookie.substring(name.length + 1) === (name + '=')){
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

//General authentication error exception thrown by authenticate and get_initial_session_status
export class AuthenticationError extends Error {
    constructor(message) {
        super(message);
        this.name = 'AuthenticationError';
    }
}

//Contains error messages specific to the login form.
export class AuthenticationFormError extends AuthenticationError {
    constructor(message, form_errors) {
        super(message);
        this.form_errors = form_errors;
        this.name = 'AuthenticationFormError';
    }
}

function check_errors(response){
    if(response.status === 502){
        throw new AuthenticationError("The gunicorn service on this machine is unreachable.")
    }
    if(!response.ok){
        throw new AuthenticationError("Unexpected error from nginx/gunicorn, see nginx and gunicorn logs.");
    }
}

export async function authenticate(user, password) {
    
    var csrftoken = getCookie('csrftoken');

    const data = new URLSearchParams();
    data.append("username", user);
    data.append("password", password);

    const response = await fetch("/api/accounts/login/", {
        method: "POST",
        cache: "no-cache",
        mode: "same-origin",
        credentials: "same-origin",
        headers:{
            "Content-Type": 'application/x-www-form-urlencoded',
            "X-CSRFToken": csrftoken
        },
        body: data
    });

    check_errors(response);
    console.log('Aquí 1');
    try{
        const result = await response.json()
    }
    catch(e){
        console.log(e)
    }

    console.log('Aquí 2');

    if("errors" in result){
        throw new AuthenticationFormError("Login Failed", result.errors);
    }

    return {...result, csrftoken: getCookie('csrftoken')};
}

export async function logout(){
    const response = await fetch("/api/accounts/logout/");
    if(response.status !== 200){
        throw new AuthenticationError("Logout Failed")
    }
}


//This function is meant to be used to get current status of the session when the page loads.
//If the user is already loggedin there is no need show the login form
export async function get_initial_session_status(){

    const response = await fetch("/api/userSessionJson/", {
        method: "GET",
        cache: "no-cache",
        mode: "same-origin",
        credentials: "same-origin"
    });

    check_errors(response);

    const result = await response.json();

    if("error" in result){
        throw new AuthenticationError(result.error);
    }

    return {...result, permissions: new Set(result.permissions), csrftoken: getCookie('csrftoken')};
}