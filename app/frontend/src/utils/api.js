import axios from 'axios/index';


const API = () => {
    const defaultOptions = {
        baseURL: "/",
        headers: {
            'Content-Type': 'application/json',
        },
    };

    const instance = axios.create(defaultOptions);

    return instance;
}

export default API();