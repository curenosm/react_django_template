import React, {Component} from 'react';
import ReactDOM from 'react-dom';

import "@babel/polyfill";
import {Router, Route, Switch, Redirect} from "react-router-dom";
import {Provider, connect} from 'react-redux';

import initialize_store from './redux/store';

import Login from './views/auth/Login.jsx';
import Users from './components/users/Users.jsx';
import Form from'./components/users/Form.jsx';
import ThemeProvider from './utils/themeContext/ThemeProvider.jsx';
import {history} from './utils/history';
import AuthLayout from './layout/AuthLayout.jsx';



const App = (props) => (
    <>
        <div className="container">
            <Users/>
            <Form/>
        </div>
    </>
);

var LoginOrRoutes = class extends React.Component {
    render () {
        if(this.props.login_status === "LoggedIn"){
            return (
                <Router history={history}>
                    <Switch>
                        <Route path="/(users|accounts)/" render={props => <App {...props} />}/>
                        <Route path="/">
                            <Redirect to="/users/"/>
                        </Route>
                        <Redirect to="/users/"/>
                    </Switch>
                </Router>
            );
        } else {
            return <AuthLayout><Login/></AuthLayout>;
        }
    }
};

LoginOrRoutes = connect(state => ({login_status: state.auth.status}))(LoginOrRoutes);

(async () => {
    const store = await initialize_store();
    ReactDOM.render(
        <Provider store={store}>
            <ThemeProvider>
                <LoginOrRoutes/>
            </ThemeProvider>
        </Provider>,
        document.getElementById("root")
    );

})().catch(err => {
    console.log(err);
});