from django.urls import re_path, path
from .views import index


urlpatterns = [
    path('', index),
    re_path(r'^.*/$', index),
]