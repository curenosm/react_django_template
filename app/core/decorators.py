import functools

from django.http import HttpResponseBadRequest
from django.contrib.auth.decorators import user_passes_test
from django.core.exceptions import PermissionDenied


# Requires user membership in at least one of the groups passed in.
def group_required(*group_names):
    def in_groups(u):
        if u.is_authenticated():
            if bool(u.groups.filter(name__in=group_names)) | u.is_superuser:
                return True
        return False
    return user_passes_test(in_groups)


# Opposite of login_required
def anonymous_required(function=None, redirect_url=None):
    if not redirect_url:
        redirect_url = settings.LOGIN_REDIRECT_URL
    
    actual_decorator = user_passes_test(
        lambda u: u.is_anonymous(),
        login_url=redirect_url
    )
    
    if function:
        return actual_decorator(function)
    return actual_decorator


# Limit view to superusers only.
def superuser_only(function):
    def wrapper(request, *args, **kwargs):
        if not request.user.is_superuser:
            raise PermissionDenied
        return function(request, *args, **kwargs)
    return wrapper


# As its name says.
def ajax_required(function):
    def wrapper(request, *args, **kwargs):
        if not request.is_ajax():
            return HttpResponseBadRequest()
        return function(request, *args, **kwargs)

    wrapper.__doc__ = function.__doc__
    wrapper.__name__ = function.__name__
    return wrapper


# This is util to time it takes to a function to execute
def timeit(function):
    def timed(*args, **kwargs):
        ts = time.time()
        result = function(*args, **kwargs)
        te = time.time()
        print(f'{method.__name__} ({args}, {kwargs}) {te - ts:2.2f} sec'
        return result
    return timed


# Util to know this
def user_can_write_a_review(function):
    """Notice this is for the new docstring not to subtitute
        the original one from the function wrapped. """
    @functools.wraps(function)
    def wrapper(request, *args, **kwargs):
        """Extra functionality"""
        return function(request, *args, **kwargs)
    return wrapper