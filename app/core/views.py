import logging

from django.shortcuts import render
from django.contrib import messages
from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout,
)

from rest_framework import status
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication
)
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from api.serializers import UserSerializer


User = get_user_model()

logger = logging.getLogger(__name__)


def index(request):
    messages.set_level(request, messages.WARNING)
    messages.warning(request, 'This is a warning message')
    messages.info(request, 'This is an info message')
    return render(request, 'base.html')


@api_view(['POST'])
def login_view(request):
    username = request.data['username']
    password = request.data['password']

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return Response(status=status.HTTP_200_OK)
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED)


@api_view(['GET'])
def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


# TODO: Test all these
class UserList(APIView):
    # permission_classes = (
    #     permissions.IsAuthenticated,
    #     permissions.IsAdminUser,
    # )

    def get(self, request):
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    # permission_classes = (
    #     permissions.IsAuthenticated,
    #     permissions.IsAdminUser,
    # )

    def get_object(self, id):
        try:
            return User.objects.get(id=id)
        except User.DoesNotExist:
            return None

    def get(self, request, id, format=None):
        obj = self.get_object(id)
        if not obj:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, id, format=None):
        obj = self.get_object(id)
        if not obj:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj.delete()
        return Response(status=status.HTTP_200_OK)