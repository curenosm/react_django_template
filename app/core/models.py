from django.contrib.auth.models import AbstractUser, Group
from django.db import models


class User(AbstractUser):
    email = models.EmailField(unique=True, null=False, blank=False)

    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Users'

    def __str__(self):
        return f'{self.username} | {self.first_name} | {self.last_name} | {self.email}'


# Adding some functionalities to base Group model
# Group.add_to_class(
#     'partner',
#     models.ForeignKey(Partner, on_delete = models.CASCADE, null=True)
# )