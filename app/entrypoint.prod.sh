#!/bin/sh

if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

python manage.py flush --no-input
python manage.py migrate

# Create a superuser by default
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@hello_django.com', 'password')" | python manage.py shell

# Advertisement to change react build
RED='\033[0;31m'
NC='\033[0m'
echo -e "${RED}Did you execute npm run build? For production${NC}"

exec "$@"