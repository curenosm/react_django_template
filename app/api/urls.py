from django.urls import path, re_path, include
from django.contrib.auth.views import LoginView

from .views import *


urlpatterns = [
    path('users/', UserList.as_view(), name='user_list'),
    path('users/<int:id>/', UserDetail.as_view(), name='user_detail'),
    path('userSessionJson/', userSessionJson, name='user_session'),
    path('accounts/login/', login_view, name='login_url'),
    path('accounts/logout/', logout_view, name='logout_url'),
]