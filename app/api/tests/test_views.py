import pytest
import json

from django.conf import settings
from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import (
    reverse,
    resolve
)

from mixer.backend.django import mixer
from rest_framework import status
from rest_framework.test import (
    APIRequestFactory,
    APIClient,
    force_authenticate,
    RequestsClient,
)

from ..views import *

User = get_user_model()


@pytest.mark.django_db
class TestViews(TestCase):

    def setUp(self):
        self.factory = APIRequestFactory()
        self.client = APIClient()
        self.admin_password = 'qwerty123'
        self.admin = User.objects.create_user(username='admin', password=self.admin_password)
