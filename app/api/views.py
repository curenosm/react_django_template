import logging

from django.contrib.auth import (
    authenticate,
    login,
    logout,
)
from django.contrib.auth import get_user_model
from django.middleware.csrf import get_token
from django.views.decorators.csrf import csrf_protect

from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.viewsets import ModelViewSet
from rest_framework import status
from rest_framework.authentication import (
    SessionAuthentication,
    BasicAuthentication
)
from rest_framework.decorators import (
    api_view,
    authentication_classes,
    permission_classes,
)

from .serializers import UserSerializer


logger = logging.getLogger(__name__)

User = get_user_model()


@api_view(['GET'])
def userSessionJson(request):
    # Tells the csrf middleware that we are using the csrf token.
    # The middleware will set the cookie in the response.
    # get_token(request)
    if request.user.is_authenticated:
        
        user_id = request.user.id
        username = request.user.get_username()
        # user_permissions = list(request.user.get_all_permissions())
        user_groups = [group.id for group in request.user.groups.all()]
        
        # for permission in user_permissions:
        #     user_permissions.append(permission)
        #     # TODO: Add groups permissions also here
        
        user_is_superuser = request.user.is_superuser
        response = {
            'id': user_id,
            'username': username,
            'isSuperuser': user_is_superuser,
            'permissions': [],
            # 'permissions': user_permissions,
            'groups': user_groups
        }
        print(response)
        return Response(response, status=status.HTTP_200_OK)
    else:
        return Response({'error': 'not logged in'}, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
def login_view(request):
    username = request.data['username']
    password = request.data['password']

    user = authenticate(request, username=username, password=password)

    if user is not None:
        login(request, user)
        return Response(status=status.HTTP_200_OK, content_type='application/json')
    else:
        return Response(status=status.HTTP_401_UNAUTHORIZED, content_type='application/json')


@api_view(['GET', 'POST'])
def logout_view(request):
    if request.user.is_authenticated:
        logout(request)
        return Response(status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


# TODO: Test all these
class UserList(APIView):
    # permission_classes = (
    #     permissions.IsAuthenticated,
    #     permissions.IsAdminUser,
    # )

    def get(self, request):
        queryset = User.objects.all()
        serializer = UserSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def post(self, request):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class UserDetail(APIView):
    # permission_classes = (
    #     permissions.IsAuthenticated,
    #     permissions.IsAdminUser,
    # )

    def get_object(self, id):
        try:
            return User.objects.get(id=id)
        except User.DoesNotExist:
            return None

    def get(self, request, id, format=None):
        obj = self.get_object(id)
        if not obj:
            return Response(status=status.HTTP_404_NOT_FOUND)
        serializer = UserSerializer(obj)
        return Response(serializer.data, status=status.HTTP_200_OK)

    def delete(self, request, id, format=None):
        obj = self.get_object(id)
        if not obj:
            return Response(status=status.HTTP_404_NOT_FOUND)
        obj.delete()
        return Response(status=status.HTTP_200_OK)