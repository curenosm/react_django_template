This starting template includes:
  - Docker-compose
  - Docker
  - Gitlab CI/CD
  - Nginx
  - Gunicorn
  - PostgreSQL
  - Django
  - Pytest
  - React

Util instructions:

  For development
  
    You can execute tests by locating at the same level
    than pytest.ini and executing:
      
      py.test
    
    You can mount the docker image using:

      docker-compose up --build

 For production:
  
    You can mount the docker image using:
    
      docker-compose -f docker-compose.prod.yml up --build
      

  NOTE:
    All documentation associated to this project can be found on the docs/ folder.